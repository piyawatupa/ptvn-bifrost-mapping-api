const axios = require('axios');
const jwt = require('jsonwebtoken');
const constant = require('../util/constant');

let authToken = { access_token: null };
const bifrostAxios = axios.create();
const signinAxios = axios.create();
const sapAxios = axios.create();
const opInterfaceAxios = axios.create();
const commancheAxios = axios.create();

commancheAxios.interceptors.request.use(async function (config) {
	config.url = `${constant.COMMANCHE_REQUEST}`;
	config.headers.ContentType = 'application/json;charset=utf-8';
	config.headers.APIKey = 'CPLI';
	config.headers.APISecret = '357ae0a821b49e743e7bbad3816144e091977175dbd76223f2e916c01d7bf82b';
	return config;
});
const commancheHttpClient = commancheAxios;

bifrostAxios.interceptors.request.use(async function (config) {
	config.url = `${constant.BIFROST_INTERFACE}${config.url}`;
	config.headers.ContentType = 'application/json;charset=utf-8';
	if (authToken.access_token !== null) {
		const tokenExpireFlag = await isTokenExpire();
		if (tokenExpireFlag) {
			authToken = await getAuthenToken();
		}
		config.headers.Authorization = 'Bearer ' + authToken.access_token;
	} else {
		authToken = await getAuthenToken();
		config.headers.Authorization = 'Bearer ' + authToken.access_token;
	}

	return config;
});
const bifrostHttpClient = bifrostAxios;

opInterfaceAxios.interceptors.request.use(async function (config) {
	config.url = `${constant.SAP_INTERFACE}${config.url}`;
	config.headers.ContentType = 'application/json;charset=utf-8';
	return config;
});
const opInterfaceHttpClient = opInterfaceAxios;

sapAxios.interceptors.request.use(async function (config) {
	config.url = `${constant.SAP_REQUEST}`;
	config.headers.ContentType = 'application/json;charset=utf-8';
	return config;
});
const sapHttpClient = sapAxios;

const isTokenExpire = async () => {
	if (authToken.access_token === null) {
		return true;
	}
	const token = authToken.access_token;
	const tokenDecode = jwt.decode(token);
	if (Date.now() >= tokenDecode.exp * 1000) {
		return true;
	}
	return false;
};

const getAuthenToken = async () => {
	let token;
	const data = 'grant_type=client_credentials&client_id=cpf&client_secret=002e48c8-ca27-4b98-951e-9acc1ade157e';
	const headers = {
		'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
	};
	await signinAxios
		.post(`https://idp-dev.pantavanij.com:8443/auth/realms/ptvn/protocol/openid-connect/token`, data, headers)
		.then(function (response) {
			token = response.data;
		})
		.catch(function (err) {
			console.log(err);
		});
	return token;
};

const sapRequest = async (requestBody) => {
	let response = await sapHttpClient
		.post(`https://donkey.cpf.co.th/RESTAdapter/TH/CALL_WRAPPER`, requestBody, {
			auth: {
				username: 'C_ONEEBD',
				password: 'cpf_1113',
			},
		})
		.catch(function (err) {
			console.log(err);
		});
	return response.data;
};

module.exports = { commancheHttpClient, bifrostHttpClient, opInterfaceHttpClient, sapHttpClient, isTokenExpire, getAuthenToken, sapRequest };
