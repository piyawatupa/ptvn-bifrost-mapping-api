const BIFROST_INTERFACE = 'http://192.168.19.131:8080/bifrost/api';
const SAP_INTERFACE = 'http://192.168.19.123:8080/api';
const SAP_REQUEST = 'https://donkey.cpf.co.th/RESTAdapter/TH/CALL_WRAPPER';
const COMMANCHE_REQUEST = 'http://58.137.64.187/bosservice/api/Receive';
const PR_TOUCHPOINT = '';
const PROXIES = '/proxies';
const PROJECT = '/project';
const MAPPING = '/mapping';

module.exports = {
	BIFROST_INTERFACE,
	SAP_INTERFACE,
	SAP_REQUEST,
	PR_TOUCHPOINT,
	PROXIES,
	PROJECT,
	MAPPING,
	COMMANCHE_REQUEST,
};
