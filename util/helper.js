export function compressText(string) {
  string = string.replace(/\"/g, "'");
  string = string.replace(/(\r\n|\n|\r|\t)/gm, '');
  string = string.replace(/\s+/g, ' ');

  return string;
}
