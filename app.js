process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
const express = require('express');
const app = express();
const port = 3001;

const cors = require('cors');
const bodyParser = require('body-parser');

const fs = require('fs');
const swaggerUi = require('swagger-ui-express');
const swaggerDocs = JSON.parse(fs.readFileSync('./swagger.json'));

const authorizationRoutes = require('./routes/authorization');
const bifrostRoutes = require('./routes/bifrost');
const prRoutes = require('./routes/pr');
const poRoutes = require('./routes/po');
const grRoutes = require('./routes/gr');
const datasyncRoutes = require('./routes/datasync');
const commanche = require('./routes/commanche')

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocs));

// Route
app.use('/api/authorization', authorizationRoutes);
app.use('/api/bifrost', bifrostRoutes);
app.use('/api/pr', prRoutes);
app.use('/api/po', poRoutes);
app.use('/api/gr', grRoutes);
app.use('/api/datasync', datasyncRoutes);
app.use('/api/commanche',commanche);

// Port listen
app.listen(port, () => {
	console.log(`http://localhost:${port}`);
});
