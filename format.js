function transform(proxyRes) {
	var responseBody = JSON.parse(proxyRes.getResultResponseBodyAsString());
	var isSuccess = 'false';
	var responseType = 'error';
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = { recordset: { cancelPoReply: '"cancelporeply"', messages: '"messages"', metaData: '"metadata"' }, record: { success: '"success"', lineNum: '"linenum"', type: '"type"', code: '"code"', description: '"description"', name: '"name"', value: '"value"' }, attr: { type: '"type"' } };
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.value + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.value + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.value + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.cancelPoReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = responseBody.E_MESSAGES.item[z].ZMESSAGE.replace('<', '&lt;').replace('>', '&gt;');
			let code = eMessageItem.match(/\\(([^)]+)\\)/) === null ? '' : eMessageItem.match(/\\(([^)]+)\\)/)[1];
			responseType = eMessageItem.search('Error') === -1 ? 'information' : 'error';
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
			messageBody = '';
		}
		xmlBody += '</recordset>';
	} else {
		isSuccess = 'true';
		responseType = 'information';
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = responseBody.E_MESSAGES.item[z].ZMESSAGE.replace('<', '&lt;').replace('>', '&gt;');
			let code = eMessageItem.match(/\\(([^)]+)\\)/) === null ? '' : eMessageItem.match(/\\(([^)]+)\\)/)[1];
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
			messageBody = '';
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	var respsonse = { data: xmlBody };
	proxyRes.setResultResponseBody(JSON.stringify(respsonse));
}
