const express = require('express');
const router = express.Router();
const { sapRequest } = require('../middleware/interceptor');
const { requestDataSync } = require('../touchpoint/datasync/request');
const { responseDataSync } = require('../touchpoint/datasync/response');

router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/datasync' + req.path);
	next();
});

// pr.get('/:action/:tablename/:keyword/:filter/:companycode/:language/:isXml*?', async (req, res) => {
router.get('/:action/:isXml*?', async (req, res) => {
	const action = req.params.action;
	// const tableName = req.params.tablename;
	// const keyword = req.params.keyword;
	// const filter = req.params.filter;
	// const companyCode = req.params.companycode;
	// const language = req.params.language;
	const isXml = req.params.isXml;
	// const body = { tableName, keyword, filter, companyCode, language };
	const body = { companyCode: '333', eId: '24', docId: "'THB'", touchPoint: 'TCURR', borg: '' };
	let requestBody = requestDataSync(body);
	if (action === 'request') {
		res.header('Content-Type', 'application/json').send(requestBody);
	} else {
		let sapResponse = await sapRequest(requestBody);
		if (typeof isXml === 'undefined') {
			let xmlBody = responseDataSync(sapResponse);
			res.header('Content-Type', 'text/xml').send(xmlBody);
		} else {
			res.send(sapResponse);
		}
	}
});

module.exports = router;
