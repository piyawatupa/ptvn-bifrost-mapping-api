const express = require('express');
const router = express.Router();
const { bifrostHttpClient } = require('../middleware/interceptor');
const constant = require('../util/constant');
router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/bifrost' + req.path);
	next();
});

//#region - Bifrsot Proxy Section

//Get Bifrost Proxies

router.get('/proxies', async (req, res) => {
	await bifrostHttpClient
		.get(`${constant.PROXIES}`)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.post('/proxies', async (req, res) => {
	await bifrostHttpClient
		.post(`${constant.PROXIES}`, req.body)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.put('/proxies', async (req, res) => {
	await bifrostHttpClient
		.put(`${constant.PROXIES}`, req.body)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});
//#endregion - Bifrsot Proxy Section

//#region - Bifrsot Project Section

//Get Bifrost Project
router.get('/project', async (req, res) => {
	await bifrostHttpClient
		.get(`${constant.PROJECT}`)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.post('/project', async (req, res) => {
	await bifrostHttpClient
		.post(`${constant.PROJECT}`, req.body)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.put('/project', async (req, res) => {
	await bifrostHttpClient
		.put(`${constant.PROJECT}`, req.body)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});
//#endregion - Bifrsot Project Section

//#region - Bifrsot Mapping Section
router.get('/mapping', async (req, res) => {
	await bifrostHttpClient
		.get(`${constant.MAPPING}`)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.post('/mapping', async (req, res) => {
	await bifrostHttpClient
		.post(`${constant.MAPPING}`, req.body)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.put('/mapping', async (req, res) => {
	await bifrostHttpClient
		.put(`${constant.MAPPING}`, req.body)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});
//#endregion - Bifrsot Mapping Section

module.exports = router;
