const express = require('express');
const router = express.Router();
const { opInterfaceHttpClient } = require('../middleware/interceptor');
const { sapRequest } = require('../middleware/interceptor');

const prTouchpoint = require('../touchpoint/pr');
const { responseCreatePr } = require('../touchpoint/pr/responseCreatePr');
const { responseCancelPr } = require('../touchpoint/pr/responseCancelPr');
const { responseAccEditPr } = require('../touchpoint/pr/responseAccEditPr');
const { responseLastApprovePr } = require('../touchpoint/pr/responseLastApprovePr');
const { responseSendReqPr } = require('../touchpoint/pr/responseSendReqPr');

router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/pr' + req.path);
	next();
});

router.get('/:prNum', async (req, res) => {
	const prNum = req.params.prNum;
	await opInterfaceHttpClient
		.get(`/pr/${prNum}/detail`)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.get('/:action/:touchpoint/:prNum/:companycode/:eid/:isXml*?', async (req, res) => {
	const action = req.params.action;
	const touchpoint = req.params.touchpoint;
	const prNum = req.params.prNum;
	const companyCode = req.params.companycode;
	const eId = req.params.eid;
	const isXml = req.params.isXml;
	const body = { companyCode, eId };
	const { createPrRequest, cancelPrRequest, changePrRequest, lastApprovePrRequest, sendPrRequest } = prTouchpoint;
	const apiResponse = await opInterfaceHttpClient.get(`/pr/${prNum}/detail`);
	let requestBody;
	switch (touchpoint) {
		case 'create':
			requestBody = createPrRequest(apiResponse.data, body);
			break;
		case 'cancel':
			requestBody = cancelPrRequest(apiResponse.data, body);
			break;
		case 'change':
			requestBody = changePrRequest(apiResponse.data, body);
			break;
		case 'lastapprove':
			requestBody = lastApprovePrRequest(apiResponse.data, body);
			break;
		case 'send':
			requestBody = sendPrRequest(apiResponse.data, body);
	}
	if (action === 'request') {
		res.header('Content-Type', 'application/json').send(requestBody);
	} else {
		let sapResponse = await sapRequest(requestBody);
		if (typeof isXml === 'undefined') {
			let xmlBody;
			switch (touchpoint) {
				case 'create':
					xmlBody = responseCreatePr(sapResponse);
					break;
				case 'cancel':
					xmlBody = responseCancelPr(sapResponse);
					break;
				case 'change':
					xmlBody = responseAccEditPr(sapResponse);
					break;
				case 'lastapprove':
					xmlBody = responseLastApprovePr(sapResponse);
					break;
				case 'send':
					xmlBody = responseSendReqPr(sapResponse);
			}
			res.header('Content-Type', 'text/xml').send(xmlBody);
		} else {
			res.send(sapResponse);
		}
	}
});

module.exports = router;
