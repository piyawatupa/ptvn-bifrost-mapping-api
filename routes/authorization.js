const express = require('express');
const router = express.Router();
const { getAuthenToken } = require('../middleware/interceptor');

router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/authorization' + req.path);
	next();
});

router.get('/signin', async (req, res) => {
	getAuthenToken().then((token) => res.status(200).send(token));
});

module.exports = router;
