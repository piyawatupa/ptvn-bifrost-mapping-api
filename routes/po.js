const express = require('express');
const router = express.Router();
const { opInterfaceHttpClient } = require('../middleware/interceptor');

const poTouchpoint = require('../touchpoint/po/index');
const { responseSendPo } = require('../touchpoint/po/responseSendPo');
const { responseActivePo } = require('../touchpoint/po/responseActivePo');
const { responseCancelPo } = require('../touchpoint/po/responseCancelPo');
const { responseChangePo } = require('../touchpoint/po/responseChangePo');
const { responseEditPo } = require('../touchpoint/po/responseEditPo');
const { sapRequest } = require('../middleware/interceptor');

router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/po' + req.path);
	next();
});

router.get('/:docNum', async (req, res) => {
	const docNum = req.params.docNum;
	await opInterfaceHttpClient
		.get(`/po/${docNum}/detail`)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.get('/:action/:touchpoint/:docNum/:companycode/:eid/:isXml*?', async (req, res) => {
	const action = req.params.action;
	const touchpoint = req.params.touchpoint;
	const docNum = req.params.docNum;
	const companyCode = req.params.companycode;
	const eId = req.params.eid;
	const isXml = req.params.isXml;
	const body = { docId: docNum, companyCode, eId };
	const { createPoRequest, changePoRequest, editPoRequest, activatedPoRequest, cancelPoRequest } = poTouchpoint;

	let apiResponse;

	if (touchpoint !== 'change') {
		apiResponse = await opInterfaceHttpClient.get(`/po/${docNum}/detail`);
	} else {
		apiResponse = await opInterfaceHttpClient.get(`/pr/${docNum}/detail`);
	}

	let requestBody;
	switch (touchpoint) {
		case 'create':
			requestBody = createPoRequest(apiResponse.data, body);
			break;
		case 'change':
			requestBody = changePoRequest(apiResponse.data, body);
			break;
		case 'active':
			requestBody = activatedPoRequest(apiResponse.data, body);
			break;
		case 'edit':
			requestBody = editPoRequest(apiResponse.data, body);
			break;
		case 'cancel':
			requestBody = cancelPoRequest(apiResponse.data, body);
			break;
	}
	if (action === 'request') {
		res.header('Content-Type', 'application/json').send(requestBody);
	} else {
		let sapResponse = await sapRequest(requestBody);
		if (typeof isXml === 'undefined') {
			let xmlBody;
			switch (touchpoint) {
				case 'create':
					xmlBody = responseSendPo(sapResponse);
					break;
				case 'change':
					xmlBody = responseChangePo(sapResponse);
					break;
				case 'active':
					xmlBody = responseActivePo(sapResponse);
					break;
				case 'edit':
					xmlBody = responseEditPo(sapResponse);
					break;
				case 'cancel':
					xmlBody = responseCancelPo(sapResponse);
					break;
			}
			res.header('Content-Type', 'text/xml').send(xmlBody);
		} else {
			res.send(sapResponse);
		}
	}
});

module.exports = router;
