const express = require('express');
const router = express.Router();
const { opInterfaceHttpClient } = require('../middleware/interceptor');
const constant = require('../util/constant');

const { grReceiveRequest } = require('../touchpoint/commanche/grReceive/request');
const { grReceiveResponse } = require('../touchpoint/commanche/grReceive/response');
const { commancheHttpClient } = require('../middleware/interceptor');

router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/commanche' + req.path);
	next();
});

router.post('/grReceive', async (req, res) => {
	let requestBody = grReceiveRequest(req);
	await commancheHttpClient
	.post(`${constant.COMMANCHE_REQUEST}`, requestBody.body)
	.then(function (response) {
		let responseXml =  grReceiveResponse(response);
		var responseBody = { data: responseXml };
		res.send(responseBody);
	})
	.catch(function (err) {
		res.send(err);
	});
});

module.exports = router;
