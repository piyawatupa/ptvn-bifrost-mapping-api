const express = require('express');
const router = express.Router();
const { opInterfaceHttpClient } = require('../middleware/interceptor');

const { grRequest } = require('../touchpoint/gr/request');
const { responseGr } = require('../touchpoint/gr/response');
const { sapRequest } = require('../middleware/interceptor');

router.use((req, res, next) => {
	console.log('Time:', new Date(Date.now()), req.method, '/gr' + req.path);
	next();
});

router.get('/:docNum', async (req, res) => {
	const docNum = req.params.docNum;
	await opInterfaceHttpClient
		.get(`/gr/${docNum}/detail`)
		.then(function (response) {
			res.send(response.data);
		})
		.catch(function (err) {
			res.send(err);
		});
});

router.get('/:action/:grId/:isXml*?', async (req, res) => {
	const action = req.params.action;
	const grId = req.params.grId;
	const isXml = req.params.isXml;
	const body = {};
	let apiResponse = await opInterfaceHttpClient.get(`/gr/${grId}/detail`);
	let requestBody = grRequest(apiResponse.data, body);
	if (action === 'request') {
		res.header('Content-Type', 'application/json').send(requestBody);
	} else {
		let sapResponse = await sapRequest(requestBody);
		if (typeof isXml === 'undefined') {
			let xmlBody = responseGr(sapResponse);
			res.header('Content-Type', 'text/xml').send(xmlBody);
		} else {
			res.send(sapResponse);
		}
	}
});

module.exports = router;
