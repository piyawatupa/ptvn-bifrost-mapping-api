function transform(proxyReq) {
	var body = JSON.parse(proxyReq.getResultRequestBody());
	var requestBody = {};

	var iDataTableItems = [
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: 'FUNCTION',
			FIELD_VALUE: 'DATA_SYNCH',
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'CachedReplyData',
			FIELD_NAME: 'tablename',
			FIELD_VALUE: body.touchPoint,
		},
		{
			COMMAND: '/NEXT',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'KEYWORD',
			FIELD_VALUE: body.touchPoint,
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'FILTER',
			FIELD_VALUE: body.docId,
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'COMPANY_CODE',
			FIELD_VALUE: body.companyCode,
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'LANGUAGE',
			FIELD_VALUE: 'en',
		},
		{
			COMMAND: '/NEXT',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	];

	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: {
			item: {
				ZMESSAGE: '',
			},
		},
		E_DATA_TABLE: {
			item: {
				COMMAND: '',
				TABLE_OR_STRUCT: '',
				FIELD_NAME: '',
				FIELD_VALUE: '',
			},
		},
	};
	proxyReq.setResultRequestBody(JSON.stringify(requestBody));
}

const requestDataSync = (body) => {
	var requestBody = {};

	var iDataTableItems = [
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: 'FUNCTION',
			FIELD_VALUE: 'DATA_SYNCH',
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'CachedReplyData',
			FIELD_NAME: 'tablename',
			FIELD_VALUE: body.touchPoint,
		},
		{
			COMMAND: '/NEXT',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'KEYWORD',
			FIELD_VALUE: body.touchPoint,
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'FILTER',
			FIELD_VALUE: body.docId,
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'COMPANY_CODE',
			FIELD_VALUE: body.companyCode,
		},
		{
			COMMAND: '/SINGLE_FIELD',
			TABLE_OR_STRUCT: 'Param',
			FIELD_NAME: 'LANGUAGE',
			FIELD_VALUE: 'en',
		},
		{
			COMMAND: '/NEXT',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	];

	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: {
			item: {
				ZMESSAGE: '',
			},
		},
		E_DATA_TABLE: {
			item: {
				COMMAND: '',
				TABLE_OR_STRUCT: '',
				FIELD_NAME: '',
				FIELD_VALUE: '',
			},
		},
	};
	return requestBody;
};
module.exports = { requestDataSync };
