function transform(proxyRes) {
	var responseBody = JSON.parse(proxyRes.getResultResponseBodyAsString());
	var tableName = responseBody.I_DATA_TABLE.item.find((item) => {
		return item.FIELD_NAME === 'tablename';
	});
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			tableName: '"' + tableName.FIELD_VALUE + '"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				id: '"id"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?>';
	xmlBody += '<recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>TaskID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPTaskID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';

	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.tableName + '>';
		var messageBody = '';
		messageBody += '<record><field name=' + recordsetListConfig.record.field.id + '></value></field>';
		messageBody += '<record><field name=' + recordsetListConfig.record.field.name + '></value></field>';
		xmlBody += messageBody;
		xmlBody += '</recordset>';
	} else {
		let eDataTable = [];
		let itemTemp = {};
		for (var c = 0; c < responseBody.E_DATA_TABLE.item.length; c++) {
			const { COMMAND, FIELD_NAME, FIELD_VALUE } = responseBody.E_DATA_TABLE.item[c];
			if (COMMAND !== '/NEXT') {
				if (FIELD_NAME === 'DATA001') {
					itemTemp.id = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA002') {
					itemTemp.value = FIELD_VALUE.toString();
					eDataTable.push(itemTemp);
				}
			} else {
				itemTemp = {};
			}
		}

		xmlBody += '<recordset name=' + recordsetListConfig.recordset.tableName + '>';
		for (var z = 0; z < eDataTable.length; z++) {
			var messageBody = '';
			var item = eDataTable[z];
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.id + '><value>' + item.id + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + item.value + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	proxyRes.setResultResponseBody(JSON.stringify(respsonse));
}

const responseDataSync = (responseBody) => {
	var tableName = responseBody.I_DATA_TABLE.item.find((item) => {
		return item.FIELD_NAME === 'tablename';
	});
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			tableName: '"' + tableName.FIELD_VALUE + '"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				id: '"id"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?>';
	xmlBody += '<recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>TaskID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPTaskID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';

	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.tableName + '>';
		var messageBody = '';
		messageBody += '<record><field name=' + recordsetListConfig.record.field.id + '></value></field>';
		messageBody += '<record><field name=' + recordsetListConfig.record.field.name + '></value></field>';
		xmlBody += messageBody;
		xmlBody += '</recordset>';
	} else {
		let eDataTable = [];
		let itemTemp = {};
		for (var c = 0; c < responseBody.E_DATA_TABLE.item.length; c++) {
			const { COMMAND, FIELD_NAME, FIELD_VALUE } = responseBody.E_DATA_TABLE.item[c];
			if (COMMAND !== '/NEXT') {
				if (FIELD_NAME === 'DATA001') {
					itemTemp.id = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA002') {
					itemTemp.value = FIELD_VALUE.toString();
					eDataTable.push(itemTemp);
				}
			} else {
				itemTemp = {};
			}
		}

		xmlBody += '<recordset name=' + recordsetListConfig.recordset.tableName + '>';
		for (var z = 0; z < eDataTable.length; z++) {
			var messageBody = '';
			var item = eDataTable[z];
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.id + '><value>' + item.id + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + item.value + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	return xmlBody;
};

module.exports = { responseDataSync };
