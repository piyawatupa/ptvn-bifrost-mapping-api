function transform(proxyRes) {
	var responseBody = proxyRes.getResultResponseBodyAsString();
	var handleResponseType = (message) => {
		if (message.trim() === 'OK') {
			return 'success';
		} else {
			return 'error';
		}
	};
	var responseType = handleResponseType(responseBody);
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			receiptReply: '"receiptreply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				success: '"success"',
				receiptNum: '"receiptnum"',
				lineNum: '"linenum"',
				type: '"type"',
				code: '"code"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field  name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field  name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.receiptReply + '><record>';	
	xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>' + (responseType === 'success' ? 'true' : 'false') + '</value></field>';
	xmlBody += '<field name=' + recordsetListConfig.record.field.receiptNum + '><value>' + (responseType === 'success' ? 'OPDocID' : '') + '</value></field>';
	xmlBody += '</record></recordset>';

	if (responseType !== 'success') {
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		var errorMessage = responseBody.replace('<', '&lt;').replace('>', '&gt;');
		var messageBody = '';
		messageBody += '<record>';
		messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
		messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
		messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + responseType + '</value></field>';
		messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + errorMessage + '</value></field>';
		messageBody += '</record>';
		xmlBody += messageBody;
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	var responseXml = { data: xmlBody };
	proxyRes.setResultResponseBodyString(JSON.stringify(responseXml));
}

const grReceiveResponse = (responseBody) => {
	var handleResponseType = (message) => {
		if (message.trim() === 'OK') {
			return 'success';
		} else {
			return 'error';
		}
	};
	var responseType = handleResponseType(responseBody.data);
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			receiptReply: '"receiptreply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				success: '"success"',
				receiptNum: '"receiptnum"',
				lineNum: '"linenum"',
				type: '"type"',
				code: '"code"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field  name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field  name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.receiptReply + '><record>';	
	xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>' + (responseType === 'success' ? 'true' : 'false') + '</value></field>';
	xmlBody += '<field name=' + recordsetListConfig.record.field.receiptNum + '><value>' + (responseType === 'success' ? 'OPDocID' : '') + '</value></field>';
	xmlBody += '</record></recordset>';

	if (responseType !== 'success') {
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		var errorMessage = responseBody.data.replace('<', '&lt;').replace('>', '&gt;');
		var messageBody = '';
		messageBody += '<record>';
		messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
		messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
		messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + responseBody.status + '</value></field>';
		messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + errorMessage + '</value></field>';
		messageBody += '</record>';
		xmlBody += messageBody;
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	return xmlBody;
};

module.exports = { grReceiveResponse };
