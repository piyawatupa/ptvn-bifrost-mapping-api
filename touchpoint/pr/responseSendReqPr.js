function transform(proxyRes) {
	var responseBody = JSON.parse(proxyRes.getResultResponseBodyAsString());
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleCode = (message) => {
		message = message.match(/\(([^)]+)\)$/);
		if (message === null) {
			return '';
		} else {
			message = message[1];
			return message.trim();
		}
	};
	var handleResponseType = (message) => {
		if (message.search('Error') > -1) {
			return 'error';
		} else if (message.search('Warning') > -1) {
			return 'warning';
		} else {
			return 'information';
		}
	};

	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			accountCheckReply: '"accountcheckreply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				success: '"success"',
				reqNum: '"reqnum"',
				lineNum: '"linenum"',
				type: '"type"',
				code: '"code"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
				distribute: '"distribute"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';

	xmlBody += '<recordset name=' + recordsetListConfig.recordset.accountCheckReply + '><record>';
	var accountAssignType = responseBody.I_DATA_TABLE.item.find((item) => {
		return item.FIELD_NAME === 'ACCT_CAT';
	});
	if (responseBody.E_SUCCESS === 'X' && accountAssignType.FIELD_VALUE === 'A') {
		var sapReqNum = responseBody.I_DATA_TABLE.item.find((item) => {
			return item.FIELD_NAME === 'REQ_NUM_C1';
		});
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value>' + sapReqNum.FIELD_VALUE + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value/></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}

		var assetBody = '';
		var lineNum = '';
		var distribNum = '';
		var assetMessageType = '';
		var assetNum = '';
		for (var n = 0; n < responseBody.E_DATA_TABLE.item.length; n++) {
			const { COMMAND, FIELD_NAME, FIELD_VALUE } = responseBody.E_DATA_TABLE.item[n];
			if (COMMAND !== '/NEXT') {
				if (FIELD_NAME === 'DATA001') {
					lineNum = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA002') {
					distribNum = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA003') {
					assetMessageType = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA004') {
					assetNum = FIELD_VALUE.toString();
				}
			} else {
				assetBody += '<record>';
				assetBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><value>' + lineNum + '</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + assetMessageType + '</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.code + '><value>ASSET</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + assetNum + '</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value>' + distribNum + '</value></field>';
				assetBody += '</record>';
				xmlBody += assetBody;
				assetBody = '';
				lineNum = '';
				distribNum = '';
				assetMessageType = '';
				assetNum = '';
			}
		}
		xmlBody += '</recordset>';
	} else if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>false</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value/></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	} else {
		var reqNum = responseBody.E_DATA_TABLE.item.find((item) => {
			return item.FIELD_NAME === 'REQ_NUM';
		});
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value>' + reqNum.FIELD_VALUE + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value/></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	var respsonse = { data: xmlBody };
	proxyRes.setResultResponseBody(JSON.stringify(respsonse));
}

const responseSendReqPr = (responseBody) => {
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleCode = (message) => {
		message = message.match(/\(([^)]+)\)$/);
		if (message === null) {
			return '';
		} else {
			message = message[1];
			return message.trim();
		}
	};
	var handleResponseType = (message) => {
		if (message.search('Error') > -1) {
			return 'error';
		} else if (message.search('Warning') > -1) {
			return 'warning';
		} else {
			return 'information';
		}
	};

	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			accountCheckReply: '"accountcheckreply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				success: '"success"',
				reqNum: '"reqnum"',
				lineNum: '"linenum"',
				type: '"type"',
				code: '"code"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
				distribute: '"distribute"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';

	xmlBody += '<recordset name=' + recordsetListConfig.recordset.accountCheckReply + '><record>';
	var accountAssignType = responseBody.I_DATA_TABLE.item.find((item) => {
		return item.FIELD_NAME === 'ACCT_CAT';
	});
	if (responseBody.E_SUCCESS === 'X' && accountAssignType.FIELD_VALUE === 'A') {
		var sapReqNum = responseBody.I_DATA_TABLE.item.find((item) => {
			return item.FIELD_NAME === 'REQ_NUM_C1';
		});
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value>' + sapReqNum.FIELD_VALUE + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value/></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}

		var assetBody = '';
		var lineNum = '';
		var distribNum = '';
		var assetMessageType = '';
		var assetNum = '';
		for (var n = 0; n < responseBody.E_DATA_TABLE.item.length; n++) {
			const { COMMAND, FIELD_NAME, FIELD_VALUE } = responseBody.E_DATA_TABLE.item[n];
			if (COMMAND !== '/NEXT') {
				if (FIELD_NAME === 'DATA001') {
					lineNum = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA002') {
					distribNum = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA003') {
					assetMessageType = FIELD_VALUE.toString();
				}
				if (FIELD_NAME === 'DATA004') {
					assetNum = FIELD_VALUE.toString();
				}
			} else {
				assetBody += '<record>';
				assetBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><value>' + lineNum + '</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + assetMessageType + '</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.code + '><value>ASSET</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + assetNum + '</value></field>';
				assetBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value>' + distribNum + '</value></field>';
				assetBody += '</record>';
				xmlBody += assetBody;
				assetBody = '';
				lineNum = '';
				distribNum = '';
				assetMessageType = '';
				assetNum = '';
			}
		}
		xmlBody += '</recordset>';
	} else if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>false</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value/></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	} else {
		var reqNum = responseBody.E_DATA_TABLE.item.find((item) => {
			return item.FIELD_NAME === 'REQ_NUM';
		});
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value>' + reqNum.FIELD_VALUE + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.distribute + '><value/></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	return xmlBody;
};

module.exports = { responseSendReqPr };
