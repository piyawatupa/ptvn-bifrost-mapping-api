function transform(proxyRes) {
	var responseBody = JSON.parse(proxyRes.getResultResponseBodyAsString());
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleCode = (message) => {
		message = message.match(/\\(([^)]+)\\)$/);
		if (message === null) {
			return '';
		} else {
			message = message[1];
			return message.trim();
		}
	};
	var handleResponseType = (message) => {
		if (message.search('Error') > -1) {
			return 'error';
		} else if (message.search('Warning') > -1) {
			return 'warning';
		} else {
			return 'information';
		}
	};
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = { recordset: { accountCheckReply: '"accountcheckreply"', messages: '"messages"', metaData: '"metadata"' }, record: { field: { success: '"success"', reqNum: '"REQ_NUM"', lineNum: '"linenum"', type: '"type"', code: '"code"', description: '"description"', name: '"name"', value: '"value"' }, attr: { type: '"type"' } } };
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.accountCheckReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>false</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	} else {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	var respsonse = { data: xmlBody };
	proxyRes.setResultResponseBody(JSON.stringify(respsonse));
}

const responseLastApprovePr = (responseBody) => {
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleCode = (message) => {
		message = message.match(/\\(([^)]+)\\)$/);
		if (message === null) {
			return '';
		} else {
			message = message[1];
			return message.trim();
		}
	};
	var handleResponseType = (message) => {
		if (message.search('Error') > -1) {
			return 'error';
		} else if (message.search('Warning') > -1) {
			return 'warning';
		} else {
			return 'information';
		}
	};
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = { recordset: { accountCheckReply: '"accountcheckreply"', messages: '"messages"', metaData: '"metadata"' }, record: { field: { success: '"success"', reqNum: '"REQ_NUM"', lineNum: '"linenum"', type: '"type"', code: '"code"', description: '"description"', name: '"name"', value: '"value"' }, attr: { type: '"type"' } } };
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.accountCheckReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>false</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	} else {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.reqNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	return xmlBody;
};

module.exports = { responseLastApprovePr };
