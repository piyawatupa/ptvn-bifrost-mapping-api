function transform(proxyReq) {
	var body = JSON.parse(proxyReq.getResultRequestBody());
	var url = 'http://192.168.19.123:8080/api/gr/' + body.docId + '/detail';
	var dataEntity = rest.get(url, {}, {}, {});
	var responseData = JSON.parse(dataEntity.getBody());
	var requestBody = {};
	var iDataTableItems = [];
	var eMessages = {
		item: {
			ZMESSAGE: '',
		},
	};
	var eDataTable = {
		item: {
			COMMAND: '',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	};
	var nextTmp = {
		COMMAND: '/NEXT',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: '',
		FIELD_VALUE: '',
	};
	var singleFieldTmp = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: 'FUNCTION',
		FIELD_VALUE: 'GOODS_RECEIPT',
	};

	var headerTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/STRUCTURE', 'GR_HEADER', fieldName, fieldValue);
		}
	};
	var itemTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/TABLE', 'GR_ITEMS', fieldName, fieldValue);
		}
	};
	var addItem = function (command, type, name, value) {
		var data = {
			COMMAND: command,
			TABLE_OR_STRUCT: type,
			FIELD_NAME: name,
			FIELD_VALUE: value,
		};
		iDataTableItems.push(data);
	};

	var isNullOrUndefined = function (data) {
		if (typeof data === 'undefined' || data === null) {
			return true;
		} else {
			return false;
		}
	};

	var handleData = function (data) {
		data = data.toString().trim() === '' ? '' : data.toString().trim();
		return data;
	};

	var header = responseData.data.header;
	iDataTableItems.push(singleFieldTmp);
	headerTemplate('PO_NUM', header.docNum);
	headerTemplate('DOC_DATE', header.documentDate);
	headerTemplate('POST_DATE', header.postingDate);
	headerTemplate('DELIV_NOTE', header.deliveryNote);
	headerTemplate('HEADER_TXT', header.comments);
	headerTemplate('BILL_LAD', header.freightBill);
	headerTemplate('SLIP_NUM', header.packingSlip);
	iDataTableItems.push(nextTmp);
	for (var z = 0; z < responseData.data.items.length; z++) {
		var item = responseData.data.items[z];
		itemTemplate('ITEM_NUM', item.lineNum);
		itemTemplate('LOCATION', '');
		itemTemplate('QUANTITY', item.receivedQty);
		itemTemplate('UOM', item.uomId);
		itemTemplate('DELIV_CMPL', item.deliveryNote);
		itemTemplate('REASON', item.reasonCode);
		itemTemplate('ITEM_TEXT', item.comments);
		itemTemplate('MOVETYPE', item.movementType);
		itemTemplate('REF_DOC', item.referNum);
		itemTemplate('CANCELFLAG', item.isCancel);
		itemTemplate('UNLOAD_PT', item.unloadingPoint);
		itemTemplate('VAL_TYPE', item.batchNumber);
		itemTemplate('STORAGELOCATION', item.storageLocale);
		iDataTableItems.push(nextTmp);
	}
	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: eMessages,
		E_DATA_TABLE: eDataTable,
	};
	proxyReq.setResultRequestBody(JSON.stringify(requestBody));
}

const grRequest = (responseData, body) => {
	var requestBody = {};
	var iDataTableItems = [];
	var eMessages = {
		item: {
			ZMESSAGE: '',
		},
	};
	var eDataTable = {
		item: {
			COMMAND: '',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	};
	var nextTmp = {
		COMMAND: '/NEXT',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: '',
		FIELD_VALUE: '',
	};
	var singleFieldTmp = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: 'FUNCTION',
		FIELD_VALUE: 'GOODS_RECEIPT',
	};

	var headerTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/STRUCTURE', 'GR_HEADER', fieldName, fieldValue);
		}
	};
	var itemTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/TABLE', 'GR_ITEMS', fieldName, fieldValue);
		}
	};
	var addItem = function (command, type, name, value) {
		var data = {
			COMMAND: command,
			TABLE_OR_STRUCT: type,
			FIELD_NAME: name,
			FIELD_VALUE: value,
		};
		iDataTableItems.push(data);
	};

	var isNullOrUndefined = function (data) {
		if (typeof data === 'undefined' || data === null) {
			return true;
		} else {
			return false;
		}
	};

	var handleData = function (data) {
		data = data.toString().trim() === '' ? '' : data.toString().trim();
		return data;
	};

	var header = responseData.data.header;
	iDataTableItems.push(singleFieldTmp);
	headerTemplate('PO_NUM', header.docNum);
	headerTemplate('DOC_DATE', header.documentDate);
	headerTemplate('POST_DATE', header.postingDate);
	headerTemplate('DELIV_NOTE', header.deliveryNote);
	headerTemplate('HEADER_TXT', header.comments);
	headerTemplate('BILL_LAD', header.freightBill);
	headerTemplate('SLIP_NUM', header.packingSlip);
	iDataTableItems.push(nextTmp);
	for (var z = 0; z < responseData.data.items.length; z++) {
		var item = responseData.data.items[z];
		itemTemplate('ITEM_NUM', item.lineNum);
		itemTemplate('LOCATION', '');
		itemTemplate('QUANTITY', item.receivedQty);
		itemTemplate('UOM', item.uomId);
		itemTemplate('DELIV_CMPL', item.deliveryComplete);
		itemTemplate('REASON', item.reasonCode);
		itemTemplate('ITEM_TEXT', item.comments);
		itemTemplate('MOVETYPE', item.movementType);
		itemTemplate('REF_DOC', item.referNum);
		itemTemplate('CANCELFLAG', item.isCancel);
		itemTemplate('UNLOAD_PT', item.unloadingPoint);
		itemTemplate('VAL_TYPE', item.batchNumber);
		itemTemplate('STORAGELOCATION', item.storageLocale);
		iDataTableItems.push(nextTmp);
	}
	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: eMessages,
		E_DATA_TABLE: eDataTable,
	};
	return requestBody;
};

module.exports = { grRequest };
