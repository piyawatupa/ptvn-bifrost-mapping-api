function transform(proxyReq) {
	var body = JSON.parse(proxyReq.getResultRequestBody());
	var url = 'http://192.168.19.123:8080/api/po/' + body.docId + '/detail';
	var dataEntity = rest.get(url, {}, {}, {});
	var responseData = JSON.parse(dataEntity.getBody());
	var requestBody = {};
	var poNum = responseData.data.header.find((item) => {
		return item.uiName === 'PONumber';
	});
	poNum = poNum.value.trim();
	var poNumField = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: 'Param',
		FIELD_NAME: 'PO_NUM',
		FIELD_VALUE: poNum,
	};
	requestBody = {
		I_DATA_TABLE: {
			item: [
				{
					COMMAND: '/SINGLE_FIELD',
					TABLE_OR_STRUCT: '',
					FIELD_NAME: 'FUNCTION',
					FIELD_VALUE: 'SET_PO_STATUS',
				},
				poNumField,
				{
					COMMAND: '/NEXT',
					TABLE_OR_STRUCT: '',
					FIELD_NAME: '',
					FIELD_VALUE: '',
				},
			],
		},
		E_MESSAGES: {
			item: {
				ZMESSAGE: '',
			},
		},
		E_DATA_TABLE: {
			item: {
				COMMAND: '',
				TABLE_OR_STRUCT: '',
				FIELD_NAME: '',
				FIELD_VALUE: '',
			},
		},
	};
	proxyReq.setResultRequestBody(JSON.stringify(requestBody));
}
const activatedPoRequest = (responseData, body) => {
	var requestBody = {};
	var poNum = responseData.data.header.find((item) => {
		return item.uiName === 'PONumber';
	});

	poNum = poNum.value.trim();
	var poNumField = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: 'Param',
		FIELD_NAME: 'PO_NUM',
		FIELD_VALUE: poNum,
	};

	requestBody = {
		I_DATA_TABLE: {
			item: [
				{
					COMMAND: '/SINGLE_FIELD',
					TABLE_OR_STRUCT: '',
					FIELD_NAME: 'FUNCTION',
					FIELD_VALUE: 'SET_PO_STATUS',
				},
				poNumField,
				{
					COMMAND: '/NEXT',
					TABLE_OR_STRUCT: '',
					FIELD_NAME: '',
					FIELD_VALUE: '',
				},
			],
		},
		E_MESSAGES: {
			item: {
				ZMESSAGE: '',
			},
		},
		E_DATA_TABLE: {
			item: {
				COMMAND: '',
				TABLE_OR_STRUCT: '',
				FIELD_NAME: '',
				FIELD_VALUE: '',
			},
		},
	};
	return requestBody;
};

module.exports = { activatedPoRequest };
