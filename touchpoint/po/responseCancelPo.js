function transform(proxyRes) {
	var responseBody = JSON.parse(proxyRes.getResultResponseBodyAsString());
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleCode = (message) => {
		message = message.match(/\(([^)]+)\)$/);
		if (message === null) {
			return '';
		} else {
			message = message[1];
			return message.trim();
		}
	};
	var handleResponseType = (message) => {
		if (message.search('Error') > -1) {
			return 'error';
		} else if (message.search('Warning') > -1) {
			return 'warning';
		} else {
			return 'information';
		}
	};

	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			cancelPoReply: '"cancelporeply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				success: '"success"',
				poNum: '"ponum"',
				lineNum: '"linenum"',
				type: '"type"',
				code: '"code"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';

	xmlBody += '<recordset name=' + recordsetListConfig.recordset.cancelPoReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>false</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.poNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	} else {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.poNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		if (responseBody.E_MESSAGES !== '') {
			for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
				var messageBody = '';
				var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
				var code = handleCode(eMessageItem);
				var responseType = handleResponseType(eMessageItem);
				messageBody += '<record>';
				messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
				messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
				messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
				messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
				messageBody += '</record>';
				xmlBody += messageBody;
			}
		} else {
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value/></field>';
			messageBody += '</record>';
		}

		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	var respsonse = { data: xmlBody };
	proxyRes.setResultResponseBody(JSON.stringify(respsonse));
}

// export function responseEditPo(responseBody) {
// var handleResponseType = (message) =>{
// 		if(eMessageItem.search('Error') > -1){
// 			return 'error'
// 		}else if (eMessageItem.search('Information') > -1){
// 			return 'information'
// 		}else{
// 			return 'warning'
// 		}
// 	}
// 	var isSuccess = 'false';
// 	var responseType = 'error';
// 	var xmlConfig = { version: '\"1.0\"', enCoding: '\"UTF-8\"', standAlone: '\"no\"' };
// 	var recordsetListConfig = {
// 		recordset: {
// 			cancelPoReply: '\"cancelporeply\"',
// 			messages: '\"messages\"',
// 			metaData: '\"metadata\"',
// 		},
// 		record: {
// 			success: '\"success\"',
// 			lineNum: '\"linenum\"',
// 			type: '\"type\"',
// 			code: '\"code\"',
// 			description: '\"description\"',
// 			name: '\"name\"',
// 			value: '\"value\"',
// 		},
// 		attr: {
// 			type: '\"type\"',
// 		},
// 	};
// 	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';

// 	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
// 	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>borgid</value></field><field  name=' + recordsetListConfig.record.value + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
// 	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>DocID</value></field><field  name=' + recordsetListConfig.record.value + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>OPDocID</value></field></record>';
// 	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>eboid</value></field><field  name=' + recordsetListConfig.record.value + '><attr name=' + recordsetListConfig.attr.type + '>string</attr><value>OPEboID</value></field></record>';
// 	xmlBody += '<record><field name=' + recordsetListConfig.record.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.value + '><value>0</value></field></record>';
// 	xmlBody += '</recordset>';

// 	xmlBody += '<recordset name=' + recordsetListConfig.recordset.cancelPoReply + '><record>';
// 	if (responseBody.E_SUCCESS === '') {
// 		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
// 		xmlBody += '</record></recordset>';
// 		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
// 		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
// 			var messageBody = '';
// 			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);let code = eMessageItem.match(/\(([^)]+)\)/) === null ? '' : eMessageItem.match(/\(([^)]+)\)/)[1].trim();
// 			responseType = eMessageItem.search('Error') === -1 ? 'information': 'error';
// 			messageBody += '<record>';
// 			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
// 			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
// 			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
// 			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
// 			messageBody += '</record>';
// 			xmlBody += messageBody;
// 			messageBody = '';
// 		}
// 		xmlBody += '</recordset>';
// 	} else {
// 		isSuccess = 'true';
// 		responseType = 'information';
// 		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
// 		xmlBody += '</record></recordset>';
// 		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
// 		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
// 			var messageBody = '';
// 			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);let code = eMessageItem.match(/\(([^)]+)\)/) === null ? '' : eMessageItem.match(/\(([^)]+)\)/)[1].trim();
// 			messageBody += '<record>';
// 			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
// 			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
// 			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
// 			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
// 			messageBody += '</record>';
// 			xmlBody += messageBody;
// 			messageBody = '';
// 		}
// 		xmlBody += '</recordset>';
// 	}
// 	xmlBody += '</recordsetList>';
// 	return xmlBody;
// }

const responseCancelPo = (responseBody) => {
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleCode = (message) => {
		message = message.match(/\(([^)]+)\)$/);
		if (message === null) {
			return '';
		} else {
			message = message[1];
			return message.trim();
		}
	};
	var handleResponseType = (message) => {
		if (message.search('Error') > -1) {
			return 'error';
		} else if (message.search('Warning') > -1) {
			return 'warning';
		} else {
			return 'information';
		}
	};

	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			cancelPoReply: '"cancelporeply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			field: {
				success: '"success"',
				poNum: '"ponum"',
				lineNum: '"linenum"',
				type: '"type"',
				code: '"code"',
				description: '"description"',
				name: '"name"',
				value: '"value"',
			},
			attr: { type: '"type"' },
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.metaData + '>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>borgid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPBorgID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>DocID</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPDocID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>eboid</value></field><field name=' + recordsetListConfig.record.field.value + '><attr name=' + recordsetListConfig.record.attr.type + '>string</attr><value>OPEboID</value></field></record>';
	xmlBody += '<record><field name=' + recordsetListConfig.record.field.name + '><value>MID</value></field><field name=' + recordsetListConfig.record.field.value + '><value>0</value></field></record>';
	xmlBody += '</recordset>';

	xmlBody += '<recordset name=' + recordsetListConfig.recordset.cancelPoReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>false</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.poNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			var code = handleCode(eMessageItem);
			var responseType = handleResponseType(eMessageItem);
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
		}
		xmlBody += '</recordset>';
	} else {
		xmlBody += '<field name=' + recordsetListConfig.record.field.success + '><value>true</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.field.poNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		if (responseBody.E_MESSAGES !== '') {
			for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
				var messageBody = '';
				var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
				var code = handleCode(eMessageItem);
				var responseType = handleResponseType(eMessageItem);
				messageBody += '<record>';
				messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
				messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value>' + responseType + '</value></field>';
				messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value>' + code + '</value></field>';
				messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value>' + eMessageItem + '</value></field>';
				messageBody += '</record>';
				xmlBody += messageBody;
			}
		} else {
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.field.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.type + '><value/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.code + '><value/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.field.description + '><value/></field>';
			messageBody += '</record>';
		}

		xmlBody += '</recordset>';
	}
	xmlBody += '</recordsetList>';
	return xmlBody;
};

module.exports = { responseCancelPo };
