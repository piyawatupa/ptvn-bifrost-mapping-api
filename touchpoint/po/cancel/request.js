function transform(proxyReq) {
	var body = JSON.parse(proxyReq.getResultRequestBody());
	var url = 'http://192.168.19.123:8080/api/po/' + body.docId + '/detail';
	var dataEntity = rest.get(url, {}, {}, {});
	var responseData = JSON.parse(dataEntity.getBody());
	var requestBody = {};
	var iDataTableItems = [];
	var eMessages = {
		item: {
			ZMESSAGE: '',
		},
	};
	var eDataTable = {
		item: {
			COMMAND: '',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	};
	var singleFieldTmp = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: 'FUNCTION',
		FIELD_VALUE: 'CANCEL_PO',
	};
	var paramTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/STRUCTURE', 'PO_HEADER', fieldName, fieldValue);
		}
	};

	var addItem = function (command, type, name, value) {
		var data = {
			COMMAND: command,
			TABLE_OR_STRUCT: type,
			FIELD_NAME: name,
			FIELD_VALUE: value,
		};
		iDataTableItems.push(data);
	};

	var isNullOrUndefined = function (data) {
		if (typeof data === 'undefined' || data === null) {
			return true;
		} else {
			return false;
		}
	};

	var handleData = function (data) {
		data = data.toString().trim() === '' ? '' : data.toString().trim();
		return data;
	};

	var headerData = responseData.data.header;
	var header = {};
	for (var h = 0; h < headerData.length; h++) {
		var headerItem = headerData[h];
		header[headerItem.uiName] = header[headerItem.uiName] || [];
		header[headerItem.uiName] = headerItem.value;
	}
	iDataTableItems.push(singleFieldTmp);
	paramTemplate('PO_NUM', header.PONumber);
	paramTemplate('LEGALCONTRACTNO', header.LegalContractNo);
	paramTemplate('CONTRACTSTATUS', header.ContractStatus);
	paramTemplate('REFERENCEPONO', header.ReferencePOno);
	paramTemplate('DETAIL_ASSET', header.SubCommand);
	paramTemplate('CONTRACTVENDOR', header.Vendor);
	paramTemplate('DUEONPAYMENT', header.DueOnPayment);
	paramTemplate('CARREGISTRATION', header.CarRegistration);
	paramTemplate('LOCATION', header.Location);
	paramTemplate('CAUSEOFCHANGE', header.CauseOfChange);
	paramTemplate('PURPOSEOFUSE', header.PurposeOfUse);
	paramTemplate('CREATEDATE', header.ContractCreateDate);
	paramTemplate('ACTIVEDATE', header.ContractActiveDate);
	paramTemplate('ENDDATE', header.ContractEndDate);
	paramTemplate('CANCELDATE', header.ContractCancelDate);
	paramTemplate('PERIODTO', header.EffectivePeriodToChange);
	paramTemplate('LEASETIME', header.ContractLeaseTime);
	paramTemplate('PAYMENTPERIOD', header.TotalPaymentPeriod);
	paramTemplate('FREQUENCY', header.FrequencyOfPayment);
	paramTemplate('NUMBEROFPAID', header.NumberOfPaidPerYear);
	paramTemplate('AMOUNT', header.AmountPerPeriod);
	paramTemplate('PLANT', header.Plant);
	paramTemplate('COSTCENTER', header.CostCenter);
	paramTemplate('BA', header.BA);
	paramTemplate('GLACCOUNT', header.GlAccount);
	paramTemplate('CREATEBY', header.CreateBy);
	paramTemplate('ENTRYDATE', header.EntryDate);
	paramTemplate('ENTRYTIME', header.EntryTime);

	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: eMessages,
		E_DATA_TABLE: eDataTable,
	};
	proxyReq.setResultRequestBody(JSON.stringify(requestBody));
}
const cancelPoRequest = (responseData, body) => {
	var requestBody = {};
	var iDataTableItems = [];
	var eMessages = {
		item: {
			ZMESSAGE: '',
		},
	};
	var eDataTable = {
		item: {
			COMMAND: '',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	};
	var singleFieldTmp = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: 'FUNCTION',
		FIELD_VALUE: 'CANCEL_PO',
	};
	var paramTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/STRUCTURE', 'PO_HEADER', fieldName, fieldValue);
		}
	};

	var addItem = function (command, type, name, value) {
		var data = {
			COMMAND: command,
			TABLE_OR_STRUCT: type,
			FIELD_NAME: name,
			FIELD_VALUE: value,
		};
		iDataTableItems.push(data);
	};

	var isNullOrUndefined = function (data) {
		if (typeof data === 'undefined' || data === null) {
			return true;
		} else {
			return false;
		}
	};

	var handleData = function (data) {
		data = data.toString().trim() === '' ? '' : data.toString().trim();
		return data;
	};

	var headerData = responseData.data.header;
	var header = {};
	for (var h = 0; h < headerData.length; h++) {
		var headerItem = headerData[h];
		header[headerItem.uiName] = header[headerItem.uiName] || [];
		header[headerItem.uiName] = headerItem.value;
	}
	iDataTableItems.push(singleFieldTmp);
	paramTemplate('PO_NUM', header.PONumber);
	paramTemplate('LEGALCONTRACTNO', header.LegalContractNo);
	paramTemplate('CONTRACTSTATUS', header.ContractStatus);
	paramTemplate('REFERENCEPONO', header.ReferencePOno);
	paramTemplate('DETAIL_ASSET', header.SubCommand);
	paramTemplate('CONTRACTVENDOR', header.Vendor);
	paramTemplate('DUEONPAYMENT', header.DueOnPayment);
	paramTemplate('CARREGISTRATION', header.CarRegistration);
	paramTemplate('LOCATION', header.Location);
	paramTemplate('CAUSEOFCHANGE', header.CauseOfChange);
	paramTemplate('PURPOSEOFUSE', header.PurposeOfUse);
	paramTemplate('CREATEDATE', header.ContractCreateDate);
	paramTemplate('ACTIVEDATE', header.ContractActiveDate);
	paramTemplate('ENDDATE', header.ContractEndDate);
	paramTemplate('CANCELDATE', header.ContractCancelDate);
	paramTemplate('PERIODTO', header.EffectivePeriodToChange);
	paramTemplate('LEASETIME', header.ContractLeaseTime);
	paramTemplate('PAYMENTPERIOD', header.TotalPaymentPeriod);
	paramTemplate('FREQUENCY', header.FrequencyOfPayment);
	paramTemplate('NUMBEROFPAID', header.NumberOfPaidPerYear);
	paramTemplate('AMOUNT', header.AmountPerPeriod);
	paramTemplate('PLANT', header.Plant);
	paramTemplate('COSTCENTER', header.CostCenter);
	paramTemplate('BA', header.BA);
	paramTemplate('GLACCOUNT', header.GlAccount);
	paramTemplate('CREATEBY', header.CreateBy);
	paramTemplate('ENTRYDATE', header.EntryDate);
	paramTemplate('ENTRYTIME', header.EntryTime);

	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: eMessages,
		E_DATA_TABLE: eDataTable,
	};
	return requestBody;
};

module.exports = { cancelPoRequest };
