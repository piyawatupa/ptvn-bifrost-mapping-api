function transform(proxyReq) {
	var body = JSON.parse(proxyReq.getResultRequestBody());
	var url = 'http://192.168.19.123:8080/api/po/' + body.docId + '/detail';
	var dataEntity = rest.get(url, {}, {}, {});
	var responseData = JSON.parse(dataEntity.getBody());
	var requestBody = {};
	var iDataTableItems = [];
	var eMessages = {
		item: {
			ZMESSAGE: '',
		},
	};
	var eDataTable = {
		item: {
			COMMAND: '',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	};
	var nextTmp = {
		COMMAND: '/NEXT',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: '',
		FIELD_VALUE: '',
	};
	var singleFieldTmp = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: 'FUNCTION',
		FIELD_VALUE: 'CREATE_PO',
	};
	var headerTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/STRUCTURE', 'PO_HEADER', fieldName, fieldValue);
		}
	};
	var itemTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/TABLE', 'PO_ITEMS', fieldName, fieldValue);
		}
	};
	var accAsgTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/TABLE', 'PO_ACCT_ASSGNS', fieldName, fieldValue);
		}
	};

	var addItem = function (command, type, name, value) {
		var data = {
			COMMAND: command,
			TABLE_OR_STRUCT: type,
			FIELD_NAME: name,
			FIELD_VALUE: value,
		};
		iDataTableItems.push(data);
	};

	var isNullOrUndefined = function (data) {
		if (typeof data === 'undefined' || data === null) {
			return true;
		} else {
			return false;
		}
	};

	var handleData = function (data) {
		data = data.toString().trim() === '' ? '' : data.toString().trim();
		return data;
	};

	var headerData = responseData.data.header;
	var header = {};
	for (var h = 0; h < headerData.length; h++) {
		var headerItem = headerData[h];
		header[headerItem.uiName] = header[headerItem.uiName] || [];
		header[headerItem.uiName] = headerItem.value;
	}
	iDataTableItems.push(singleFieldTmp);
	headerTemplate('PO_NUM_C1', header.PONumber);
	headerTemplate('SUBCOMMAND', header.SubCommand);
	headerTemplate('PO_DATE', header.OrderedOnDate);
	headerTemplate('COMPANY_CODE', header.CompanyCode);
	headerTemplate('PUR_ORG', 'PU01');
	headerTemplate('VENDOR', header.Vendor);
	headerTemplate('PR_NUM', header.PRNumber);
	headerTemplate('PAYMENT_TERM', header.PaymentTerm);
	headerTemplate('LONG_TEXT1', header.LongText1);
	headerTemplate('LEGALCONTRACTNO', header.LegalContractNo);
	headerTemplate('CONTRACTSTATUS', header.ContractStatus);
	headerTemplate('REFERENCEPONO', header.ReferencePOno);
	headerTemplate('DETAIL_ASSET', header.DetailOfUnderlyingAsset);
	headerTemplate('CONTRACTVENDOR', header.Vendor);
	headerTemplate('DUEONPAYMENT', header.DueOnPayment);
	headerTemplate('CARREGISTRATION', header.CarRegistration);
	headerTemplate('LOCATION', header.Location);
	headerTemplate('CAUSEOFCHANGE', header.CauseOfChange);
	headerTemplate('PURPOSEOFUSE', header.PurposeOfUse);
	headerTemplate('CREATEDATE', header.ContractCreateDate);
	headerTemplate('ACTIVEDATE', header.ContractActiveDate);
	headerTemplate('ENDDATE', header.ContractEndDate);
	headerTemplate('CANCELDATE', header.ContractCancelDate);
	headerTemplate('PERIODTO', header.EffectivePeriodToChange);
	headerTemplate('LEASETIME', header.ContractLeaseTime);
	headerTemplate('PAYMENTPERIOD', header.TotalPaymentPeriod);
	headerTemplate('FREQUENCY', header.FrequencyOfPayment);
	headerTemplate('NUMBEROFPAID', header.NumberOfPaidPerYear);
	headerTemplate('AMOUNT', header.AmountPerPeriod);
	headerTemplate('PLANT', header.Plant);
	headerTemplate('COSTCENTER', header.CostCenter);
	headerTemplate('BA', header.BA);
	headerTemplate('GLACCOUNT', header.GlAccount);
	headerTemplate('CREATEBY', header.CreateBy);
	headerTemplate('ENTRYDATE', header.EntryDate);
	headerTemplate('ENTRYTIME', header.EntryTime);
	iDataTableItems.push(nextTmp);

	for (var z = 0; z < responseData.data.items.length; z++) {
		var item = responseData.data.items[z].item;
		var accAsgData = responseData.data.items[z].accountAssignments;
		var accAsg = {};

		for (var p = 0; p < accAsgData.length; p++) {
			var accAsgItem = accAsgData[p];
			accAsg[accAsgItem.name] = accAsg[accAsgItem.name] || [];
			accAsg[accAsgItem.name] = accAsgItem.value;
		}

		itemTemplate('ITEM_NUM', item.lineNum);
		itemTemplate('PLANT', accAsg.plant);
		itemTemplate('MATERIAL', item.buyerPartNum);
		itemTemplate('ITEM_DESC', item.partDesc);
		itemTemplate('QUANTITY', item.orderQty);
		itemTemplate('UOM', item.uomId);
		itemTemplate('PRICE', item.unitPrice);
		itemTemplate('CURRENCY_CODE', item.currencyCode);
		itemTemplate('VENDOR_MAT', item.supplierPartNum);
		itemTemplate('DELIV_DATE', item.deliverByDate);
		itemTemplate('SHIP_INST', item.shipMethodId);
		itemTemplate('PR_LINE_NUM', item.prLineNum);
		itemTemplate('LONGDESC', item.userDefined3);
		itemTemplate('CPMNUM', '');
		iDataTableItems.push(nextTmp);
		accAsgTemplate('ITEM_NUM', item.lineNum);
		accAsgTemplate('ACCT_CAT', accAsg.acctassign);
		accAsgTemplate('PLANT', accAsg.plant);
		accAsgTemplate('PUR_GRP', accAsg.purgroup);
		accAsgTemplate('COSTCENTER', accAsg.costcenter);
		accAsgTemplate('PROFIT_CTR', accAsg.profitcenter);
		accAsgTemplate('GL_ACCOUNT', accAsg.glaccount);
		accAsgTemplate('AUC_CODE', accAsg.auc);
		accAsgTemplate('INT_ORDER', accAsg.intorder);
		accAsgTemplate('MATL_GRP', accAsg.matgroup);
		accAsgTemplate('MATL_CODE', accAsg.matcode);
		accAsgTemplate('VENDOR', accAsg.vendor);
		accAsgTemplate('FREE_CHARGE', accAsg.freeflag);
		accAsgTemplate('GR_FLAG', accAsg.grflag);
		accAsgTemplate('GRIV_FLAG', accAsg.grbasediv);
		accAsgTemplate('TAX_CODE', accAsg.taxcode);
		accAsgTemplate('FIX_RATE', accAsg.fixrate);
		accAsgTemplate('DOC_TYPE', accAsg.doctype);
		accAsgTemplate('FEED_MEDICINE', accAsg.feedmedicine);
		accAsgTemplate('PURUNIT1', accAsg.purunit1);
		accAsgTemplate('PURUNIT3', accAsg.purunit3);
		accAsgTemplate('SALE1PERSON', accAsg.sale1person);
		accAsgTemplate('CUSTOMER', accAsg.customer);
		accAsgTemplate('CREDCONTRAREA', accAsg.credcontrarea);
		accAsgTemplate('REFERENCE', accAsg.reference);
		accAsgTemplate('WEIGHT', accAsg.weight);
		accAsgTemplate('ORDER_UNIT', '');
		accAsgTemplate('HEADERDISTYPE', accAsg.headerdistype);
		accAsgTemplate('HEADERCNTY1', accAsg.headercnty1);
		accAsgTemplate('HEADERCNTY2', accAsg.headercnty2);
		accAsgTemplate('HEADERCNTY3', accAsg.headercnty3);
		accAsgTemplate('HEADERCNTY4', accAsg.headercnty4);
		accAsgTemplate('HEADERCNTY5', accAsg.headercnty5);
		accAsgTemplate('HEADERCNTY6', accAsg.headercnty6);
		accAsgTemplate('HEADERCNTY7', accAsg.headercnty7);
		accAsgTemplate('HEADERCNTY8', accAsg.headercnty8);
		accAsgTemplate('HEADERCNTY9', accAsg.headercnty9);
		accAsgTemplate('HEADERCNTY10', accAsg.headercnty10);
		accAsgTemplate('HEADERDISAMT', accAsg.headerdisamt);
		accAsgTemplate('HEADERRATE1', accAsg.headerrate1);
		accAsgTemplate('HEADERRATE2', accAsg.headerrate2);
		accAsgTemplate('HEADERRATE3', accAsg.headerrate3);
		accAsgTemplate('HEADERRATE4', accAsg.headerrate4);
		accAsgTemplate('HEADERRATE5', accAsg.headerrate5);
		accAsgTemplate('HEADERRATE6', accAsg.headerrate6);
		accAsgTemplate('HEADERRATE7', accAsg.headerrate7);
		accAsgTemplate('HEADERRATE8', accAsg.headerrate8);
		accAsgTemplate('HEADERRATE9', accAsg.headerrate9);
		accAsgTemplate('HEADERRATE10', accAsg.headerrate10);
		accAsgTemplate('HEADERDISCURR', accAsg.headerdiscurr);
		accAsgTemplate('HEADERCURR1', accAsg.headercurr1);
		accAsgTemplate('HEADERCURR2', accAsg.headercurr2);
		accAsgTemplate('HEADERCURR3', accAsg.headercurr3);
		accAsgTemplate('HEADERCURR4', accAsg.headercurr4);
		accAsgTemplate('HEADERCURR5', accAsg.headercurr5);
		accAsgTemplate('HEADERCURR6', accAsg.headercurr6);
		accAsgTemplate('HEADERCURR7', accAsg.headercurr7);
		accAsgTemplate('HEADERCURR8', accAsg.headercurr8);
		accAsgTemplate('HEADERCURR9', accAsg.headercurr9);
		accAsgTemplate('HEADERCURR10', accAsg.headercurr10);
		accAsgTemplate('HEADERPER1', accAsg.headerper1);
		accAsgTemplate('HEADERPER2', accAsg.headerper2);
		accAsgTemplate('HEADERPER3', accAsg.headerper3);
		accAsgTemplate('HEADERPER4', accAsg.headerper4);
		accAsgTemplate('HEADERPER5', accAsg.headerper5);
		accAsgTemplate('HEADERPER6', accAsg.headerper6);
		accAsgTemplate('HEADERPER7', accAsg.headerper7);
		accAsgTemplate('HEADERPER8', accAsg.headerper8);
		accAsgTemplate('HEADERPER9', accAsg.headerper9);
		accAsgTemplate('HEADERPER10', accAsg.headerper10);
		accAsgTemplate('HEADERUOM1', accAsg.headeruom1);
		accAsgTemplate('HEADERUOM2', accAsg.headeruom2);
		accAsgTemplate('HEADERUOM3', accAsg.headeruom3);
		accAsgTemplate('HEADERUOM4', accAsg.headeruom4);
		accAsgTemplate('HEADERUOM5', accAsg.headeruom5);
		accAsgTemplate('HEADERUOM6', accAsg.headeruom6);
		accAsgTemplate('HEADERUOM7', accAsg.headeruom7);
		accAsgTemplate('HEADERUOM8', accAsg.headeruom8);
		accAsgTemplate('HEADERUOM9', accAsg.headeruom9);
		accAsgTemplate('HEADERUOM10', accAsg.headeruom10);
		accAsgTemplate('HEADERVENDOR1', accAsg.headervendor1);
		accAsgTemplate('HEADERVENDOR2', accAsg.headervendor2);
		accAsgTemplate('HEADERVENDOR3', accAsg.headervendor3);
		accAsgTemplate('HEADERVENDOR4', accAsg.headervendor4);
		accAsgTemplate('HEADERVENDOR5', accAsg.headervendor5);
		accAsgTemplate('HEADERVENDOR6', accAsg.headervendor6);
		accAsgTemplate('HEADERVENDOR7', accAsg.headervendor7);
		accAsgTemplate('HEADERVENDOR8', accAsg.headervendor8);
		accAsgTemplate('HEADERVENDOR9', accAsg.headervendor9);
		accAsgTemplate('HEADERVENDOR10', accAsg.headervendor10);
		accAsgTemplate('DETAILCNTY1', accAsg.detailcnty1);
		accAsgTemplate('DETAILCNTY2', accAsg.detailcnty2);
		accAsgTemplate('DETAILCNTY3', accAsg.detailcnty3);
		accAsgTemplate('DETAILCNTY4', accAsg.detailcnty4);
		accAsgTemplate('DETAILCNTY5', accAsg.detailcnty5);
		accAsgTemplate('DETAILCNTY6', accAsg.detailcnty6);
		accAsgTemplate('DETAILCNTY7', accAsg.detailcnty7);
		accAsgTemplate('DETAILCNTY8', accAsg.detailcnty8);
		accAsgTemplate('DETAILCNTY9', accAsg.detailcnty9);
		accAsgTemplate('DETAILCNTY10', accAsg.detailcnty10);
		accAsgTemplate('DETAILRATE1', accAsg.detailrate1);
		accAsgTemplate('DETAILRATE2', accAsg.detailrate2);
		accAsgTemplate('DETAILRATE3', accAsg.detailrate3);
		accAsgTemplate('DETAILRATE4', accAsg.detailrate4);
		accAsgTemplate('DETAILRATE5', accAsg.detailrate5);
		accAsgTemplate('DETAILRATE6', accAsg.detailrate6);
		accAsgTemplate('DETAILRATE7', accAsg.detailrate7);
		accAsgTemplate('DETAILRATE8', accAsg.detailrate8);
		accAsgTemplate('DETAILRATE9', accAsg.detailrate9);
		accAsgTemplate('DETAILRATE10', accAsg.detailrate10);
		accAsgTemplate('DETAILCURR1', accAsg.detailcurr1);
		accAsgTemplate('DETAILCURR2', accAsg.detailcurr2);
		accAsgTemplate('DETAILCURR3', accAsg.detailcurr3);
		accAsgTemplate('DETAILCURR4', accAsg.detailcurr4);
		accAsgTemplate('DETAILCURR5', accAsg.detailcurr5);
		accAsgTemplate('DETAILCURR6', accAsg.detailcurr6);
		accAsgTemplate('DETAILCURR7', accAsg.detailcurr7);
		accAsgTemplate('DETAILCURR8', accAsg.detailcurr8);
		accAsgTemplate('DETAILCURR9', accAsg.detailcurr9);
		accAsgTemplate('DETAILCURR10', accAsg.detailcurr10);
		accAsgTemplate('DETAILPER1', accAsg.detailper1);
		accAsgTemplate('DETAILPER2', accAsg.detailper2);
		accAsgTemplate('DETAILPER3', accAsg.detailper3);
		accAsgTemplate('DETAILPER4', accAsg.detailper4);
		accAsgTemplate('DETAILPER5', accAsg.detailper5);
		accAsgTemplate('DETAILPER6', accAsg.detailper6);
		accAsgTemplate('DETAILPER7', accAsg.detailper7);
		accAsgTemplate('DETAILPER8', accAsg.detailper8);
		accAsgTemplate('DETAILPER9', accAsg.detailper9);
		accAsgTemplate('DETAILPER10', accAsg.detailper10);
		accAsgTemplate('DETAILUOM1', accAsg.detailuom1);
		accAsgTemplate('DETAILUOM2', accAsg.detailuom2);
		accAsgTemplate('DETAILUOM3', accAsg.detailuom3);
		accAsgTemplate('DETAILUOM4', accAsg.detailuom4);
		accAsgTemplate('DETAILUOM5', accAsg.detailuom5);
		accAsgTemplate('DETAILUOM6', accAsg.detailuom6);
		accAsgTemplate('DETAILUOM7', accAsg.detailuom7);
		accAsgTemplate('DETAILUOM8', accAsg.detailuom8);
		accAsgTemplate('DETAILUOM9', accAsg.detailuom9);
		accAsgTemplate('DETAILUOM10', accAsg.detailuom10);
		accAsgTemplate('DETAILVENDOR1', accAsg.detailvendor1);
		accAsgTemplate('DETAILVENDOR2', accAsg.detailvendor2);
		accAsgTemplate('DETAILVENDOR3', accAsg.detailvendor3);
		accAsgTemplate('DETAILVENDOR4', accAsg.detailvendor4);
		accAsgTemplate('DETAILVENDOR5', accAsg.detailvendor5);
		accAsgTemplate('DETAILVENDOR6', accAsg.detailvendor6);
		accAsgTemplate('DETAILVENDOR7', accAsg.detailvendor7);
		accAsgTemplate('DETAILVENDOR8', accAsg.detailvendor8);
		accAsgTemplate('DETAILVENDOR9', accAsg.detailvendor9);
		accAsgTemplate('DETAILVENDOR10', accAsg.detailvendor10);
		accAsgTemplate('INCOTERMS1', accAsg.incoterms1);
		accAsgTemplate('INCOTERMS2', accAsg.incoterms2);
		accAsgTemplate('PRREFLINE', accAsg.prrefline);
		accAsgTemplate('CRITERIA1', accAsg.criteria1);
		accAsgTemplate('DEPARTMENT', accAsg.department);
		accAsgTemplate('REMARK', accAsg.headerremark);
		accAsgTemplate('REMARK1', accAsg.headerremark1);
		accAsgTemplate('REMARK2', accAsg.headerremark2);
		accAsgTemplate('REMARK3', accAsg.headerremark3);
		accAsgTemplate('NOTE', accAsg.headernote);
		accAsgTemplate('CONTACTNUM', accAsg.contactnumber);
		accAsgTemplate('REQUESTER', accAsg.requester);
		accAsgTemplate('SAVING_METHOD', accAsg.savingmethod);
		accAsgTemplate('YOUR_REF', accAsg.yourreference);
		accAsgTemplate('SHIPPING_INST', accAsg.shippinginstruction);
		accAsgTemplate('TERMS_OR_PAYMENT', accAsg.termofpayment);
		accAsgTemplate('OTHER_TERMS_COND', accAsg.othertermscondition);
		accAsgTemplate('OVER_DELIV', accAsg.overdelivtol);
		accAsgTemplate('UNDER_DELIV', accAsg.underdelivtol);
		accAsgTemplate('MAT_PO_TEXT', accAsg.matpotext);
		accAsgTemplate('SHIPPING_CHANNEL', accAsg.shippingchannel);
		accAsgTemplate('NET_WEIGHT', accAsg.netweight);
		accAsgTemplate('GROSS_WEIGHT', accAsg.grossweight);
		accAsgTemplate('UNIT_WEIGHT', accAsg.unitweight);
		accAsgTemplate('SUB_NUMBER', accAsg.subnumber);
		accAsgTemplate('CR', accAsg.cr);
		accAsgTemplate('REASON', accAsg.reason);
		accAsgTemplate('LEGAL_FLAG', accAsg.legalcontractflag);
		accAsgTemplate('LEGAL_CONTRACT_NO', accAsg.legalcontractno);
		accAsgTemplate('UNLOAD_PT', accAsg.unloadingpoint);
		accAsgTemplate('CAUNIT', 'ML');
		iDataTableItems.push(nextTmp);
	}
	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: eMessages,
		E_DATA_TABLE: eDataTable,
	};
	proxyReq.setResultRequestBody(JSON.stringify(requestBody));
}

const createPoRequest = (responseData, body) => {
	var requestBody = {};
	var iDataTableItems = [];
	var eMessages = {
		item: {
			ZMESSAGE: '',
		},
	};
	var eDataTable = {
		item: {
			COMMAND: '',
			TABLE_OR_STRUCT: '',
			FIELD_NAME: '',
			FIELD_VALUE: '',
		},
	};
	var nextTmp = {
		COMMAND: '/NEXT',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: '',
		FIELD_VALUE: '',
	};
	var singleFieldTmp = {
		COMMAND: '/SINGLE_FIELD',
		TABLE_OR_STRUCT: '',
		FIELD_NAME: 'FUNCTION',
		FIELD_VALUE: 'CREATE_PO',
	};
	var headerTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/STRUCTURE', 'PO_HEADER', fieldName, fieldValue);
		}
	};
	var itemTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/TABLE', 'PO_ITEMS', fieldName, fieldValue);
		}
	};
	var accAsgTemplate = function (fieldName, fieldValue) {
		if (!isNullOrUndefined(fieldValue)) {
			fieldValue = handleData(fieldValue);
			addItem('/TABLE', 'PO_ACCT_ASSGNS', fieldName, fieldValue);
		}
	};

	var addItem = function (command, type, name, value) {
		var data = {
			COMMAND: command,
			TABLE_OR_STRUCT: type,
			FIELD_NAME: name,
			FIELD_VALUE: value,
		};
		iDataTableItems.push(data);
	};

	var isNullOrUndefined = function (data) {
		if (typeof data === 'undefined' || data === null) {
			return true;
		} else {
			return false;
		}
	};

	var handleData = function (data) {
		data = data.toString().trim() === '' ? '' : data.toString().trim();
		return data;
	};

	var headerData = responseData.data.header;
	var header = {};
	for (var h = 0; h < headerData.length; h++) {
		var headerItem = headerData[h];
		header[headerItem.uiName] = header[headerItem.uiName] || [];
		header[headerItem.uiName] = headerItem.value;
	}
	iDataTableItems.push(singleFieldTmp);
	headerTemplate('PO_NUM_C1', header.PONumber);
	headerTemplate('SUBCOMMAND', header.SubCommand);
	headerTemplate('PO_DATE', header.OrderedOnDate);
	headerTemplate('COMPANY_CODE', header.CompanyCode);
	headerTemplate('PUR_ORG', 'PU01');
	headerTemplate('VENDOR', header.Vendor);
	headerTemplate('PR_NUM', header.PRNumber);
	headerTemplate('PAYMENT_TERM', header.PaymentTerm);
	headerTemplate('LONG_TEXT1', header.LongText1);
	headerTemplate('LEGALCONTRACTNO', header.LegalContractNo);
	headerTemplate('CONTRACTSTATUS', header.ContractStatus);
	headerTemplate('REFERENCEPONO', header.ReferencePOno);
	headerTemplate('DETAIL_ASSET', header.DetailOfUnderlyingAsset);
	headerTemplate('CONTRACTVENDOR', header.Vendor);
	headerTemplate('DUEONPAYMENT', header.DueOnPayment);
	headerTemplate('CARREGISTRATION', header.CarRegistration);
	headerTemplate('LOCATION', header.Location);
	headerTemplate('CAUSEOFCHANGE', header.CauseOfChange);
	headerTemplate('PURPOSEOFUSE', header.PurposeOfUse);
	headerTemplate('CREATEDATE', header.ContractCreateDate);
	headerTemplate('ACTIVEDATE', header.ContractActiveDate);
	headerTemplate('ENDDATE', header.ContractEndDate);
	headerTemplate('CANCELDATE', header.ContractCancelDate);
	headerTemplate('PERIODTO', header.EffectivePeriodToChange);
	headerTemplate('LEASETIME', header.ContractLeaseTime);
	headerTemplate('PAYMENTPERIOD', header.TotalPaymentPeriod);
	headerTemplate('FREQUENCY', header.FrequencyOfPayment);
	headerTemplate('NUMBEROFPAID', header.NumberOfPaidPerYear);
	headerTemplate('AMOUNT', header.AmountPerPeriod);
	headerTemplate('PLANT', header.Plant);
	headerTemplate('COSTCENTER', header.CostCenter);
	headerTemplate('BA', header.BA);
	headerTemplate('GLACCOUNT', header.GlAccount);
	headerTemplate('CREATEBY', header.CreateBy);
	headerTemplate('ENTRYDATE', header.EntryDate);
	headerTemplate('ENTRYTIME', header.EntryTime);
	iDataTableItems.push(nextTmp);

	for (var z = 0; z < responseData.data.items.length; z++) {
		var item = responseData.data.items[z].item;
		var accAsgData = responseData.data.items[z].accountAssignments;
		var accAsg = {};

		for (var p = 0; p < accAsgData.length; p++) {
			var accAsgItem = accAsgData[p];
			accAsg[accAsgItem.name] = accAsg[accAsgItem.name] || [];
			accAsg[accAsgItem.name] = accAsgItem.value;
		}

		itemTemplate('ITEM_NUM', item.lineNum);
		itemTemplate('PLANT', accAsg.plant);
		itemTemplate('MATERIAL', item.buyerPartNum);
		itemTemplate('ITEM_DESC', item.partDesc);
		itemTemplate('QUANTITY', item.orderQty);
		itemTemplate('UOM', item.uomId);
		itemTemplate('PRICE', item.unitPrice);
		itemTemplate('CURRENCY_CODE', item.currencyCode);
		itemTemplate('VENDOR_MAT', item.supplierPartNum);
		itemTemplate('DELIV_DATE', item.deliverByDate);
		itemTemplate('SHIP_INST', item.shipMethodId);
		itemTemplate('PR_LINE_NUM', item.prLineNum);
		itemTemplate('LONGDESC', item.userDefined3);
		itemTemplate('CPMNUM', '');
		iDataTableItems.push(nextTmp);
		accAsgTemplate('ITEM_NUM', item.lineNum);
		accAsgTemplate('ACCT_CAT', accAsg.acctassign);
		accAsgTemplate('PLANT', accAsg.plant);
		accAsgTemplate('PUR_GRP', accAsg.purgroup);
		accAsgTemplate('COSTCENTER', accAsg.costcenter);
		accAsgTemplate('PROFIT_CTR', accAsg.profitcenter);
		accAsgTemplate('GL_ACCOUNT', accAsg.glaccount);
		accAsgTemplate('AUC_CODE', accAsg.auc);
		accAsgTemplate('INT_ORDER', accAsg.intorder);
		accAsgTemplate('MATL_GRP', accAsg.matgroup);
		accAsgTemplate('MATL_CODE', accAsg.matcode);
		accAsgTemplate('VENDOR', accAsg.vendor);
		accAsgTemplate('FREE_CHARGE', accAsg.freeflag);
		accAsgTemplate('GR_FLAG', accAsg.grflag);
		accAsgTemplate('GRIV_FLAG', accAsg.grbasediv);
		accAsgTemplate('TAX_CODE', accAsg.taxcode);
		accAsgTemplate('FIX_RATE', accAsg.fixrate);
		accAsgTemplate('DOC_TYPE', accAsg.doctype);
		accAsgTemplate('FEED_MEDICINE', accAsg.feedmedicine);
		accAsgTemplate('PURUNIT1', accAsg.purunit1);
		accAsgTemplate('PURUNIT3', accAsg.purunit3);
		accAsgTemplate('SALE1PERSON', accAsg.sale1person);
		accAsgTemplate('CUSTOMER', accAsg.customer);
		accAsgTemplate('CREDCONTRAREA', accAsg.credcontrarea);
		accAsgTemplate('REFERENCE', accAsg.reference);
		accAsgTemplate('WEIGHT', accAsg.weight);
		accAsgTemplate('ORDER_UNIT', '');
		accAsgTemplate('HEADERDISTYPE', accAsg.headerdistype);
		accAsgTemplate('HEADERCNTY1', accAsg.headercnty1);
		accAsgTemplate('HEADERCNTY2', accAsg.headercnty2);
		accAsgTemplate('HEADERCNTY3', accAsg.headercnty3);
		accAsgTemplate('HEADERCNTY4', accAsg.headercnty4);
		accAsgTemplate('HEADERCNTY5', accAsg.headercnty5);
		accAsgTemplate('HEADERCNTY6', accAsg.headercnty6);
		accAsgTemplate('HEADERCNTY7', accAsg.headercnty7);
		accAsgTemplate('HEADERCNTY8', accAsg.headercnty8);
		accAsgTemplate('HEADERCNTY9', accAsg.headercnty9);
		accAsgTemplate('HEADERCNTY10', accAsg.headercnty10);
		accAsgTemplate('HEADERDISAMT', accAsg.headerdisamt);
		accAsgTemplate('HEADERRATE1', accAsg.headerrate1);
		accAsgTemplate('HEADERRATE2', accAsg.headerrate2);
		accAsgTemplate('HEADERRATE3', accAsg.headerrate3);
		accAsgTemplate('HEADERRATE4', accAsg.headerrate4);
		accAsgTemplate('HEADERRATE5', accAsg.headerrate5);
		accAsgTemplate('HEADERRATE6', accAsg.headerrate6);
		accAsgTemplate('HEADERRATE7', accAsg.headerrate7);
		accAsgTemplate('HEADERRATE8', accAsg.headerrate8);
		accAsgTemplate('HEADERRATE9', accAsg.headerrate9);
		accAsgTemplate('HEADERRATE10', accAsg.headerrate10);
		accAsgTemplate('HEADERDISCURR', accAsg.headerdiscurr);
		accAsgTemplate('HEADERCURR1', accAsg.headercurr1);
		accAsgTemplate('HEADERCURR2', accAsg.headercurr2);
		accAsgTemplate('HEADERCURR3', accAsg.headercurr3);
		accAsgTemplate('HEADERCURR4', accAsg.headercurr4);
		accAsgTemplate('HEADERCURR5', accAsg.headercurr5);
		accAsgTemplate('HEADERCURR6', accAsg.headercurr6);
		accAsgTemplate('HEADERCURR7', accAsg.headercurr7);
		accAsgTemplate('HEADERCURR8', accAsg.headercurr8);
		accAsgTemplate('HEADERCURR9', accAsg.headercurr9);
		accAsgTemplate('HEADERCURR10', accAsg.headercurr10);
		accAsgTemplate('HEADERPER1', accAsg.headerper1);
		accAsgTemplate('HEADERPER2', accAsg.headerper2);
		accAsgTemplate('HEADERPER3', accAsg.headerper3);
		accAsgTemplate('HEADERPER4', accAsg.headerper4);
		accAsgTemplate('HEADERPER5', accAsg.headerper5);
		accAsgTemplate('HEADERPER6', accAsg.headerper6);
		accAsgTemplate('HEADERPER7', accAsg.headerper7);
		accAsgTemplate('HEADERPER8', accAsg.headerper8);
		accAsgTemplate('HEADERPER9', accAsg.headerper9);
		accAsgTemplate('HEADERPER10', accAsg.headerper10);
		accAsgTemplate('HEADERUOM1', accAsg.headeruom1);
		accAsgTemplate('HEADERUOM2', accAsg.headeruom2);
		accAsgTemplate('HEADERUOM3', accAsg.headeruom3);
		accAsgTemplate('HEADERUOM4', accAsg.headeruom4);
		accAsgTemplate('HEADERUOM5', accAsg.headeruom5);
		accAsgTemplate('HEADERUOM6', accAsg.headeruom6);
		accAsgTemplate('HEADERUOM7', accAsg.headeruom7);
		accAsgTemplate('HEADERUOM8', accAsg.headeruom8);
		accAsgTemplate('HEADERUOM9', accAsg.headeruom9);
		accAsgTemplate('HEADERUOM10', accAsg.headeruom10);
		accAsgTemplate('HEADERVENDOR1', accAsg.headervendor1);
		accAsgTemplate('HEADERVENDOR2', accAsg.headervendor2);
		accAsgTemplate('HEADERVENDOR3', accAsg.headervendor3);
		accAsgTemplate('HEADERVENDOR4', accAsg.headervendor4);
		accAsgTemplate('HEADERVENDOR5', accAsg.headervendor5);
		accAsgTemplate('HEADERVENDOR6', accAsg.headervendor6);
		accAsgTemplate('HEADERVENDOR7', accAsg.headervendor7);
		accAsgTemplate('HEADERVENDOR8', accAsg.headervendor8);
		accAsgTemplate('HEADERVENDOR9', accAsg.headervendor9);
		accAsgTemplate('HEADERVENDOR10', accAsg.headervendor10);
		accAsgTemplate('DETAILCNTY1', accAsg.detailcnty1);
		accAsgTemplate('DETAILCNTY2', accAsg.detailcnty2);
		accAsgTemplate('DETAILCNTY3', accAsg.detailcnty3);
		accAsgTemplate('DETAILCNTY4', accAsg.detailcnty4);
		accAsgTemplate('DETAILCNTY5', accAsg.detailcnty5);
		accAsgTemplate('DETAILCNTY6', accAsg.detailcnty6);
		accAsgTemplate('DETAILCNTY7', accAsg.detailcnty7);
		accAsgTemplate('DETAILCNTY8', accAsg.detailcnty8);
		accAsgTemplate('DETAILCNTY9', accAsg.detailcnty9);
		accAsgTemplate('DETAILCNTY10', accAsg.detailcnty10);
		accAsgTemplate('DETAILRATE1', accAsg.detailrate1);
		accAsgTemplate('DETAILRATE2', accAsg.detailrate2);
		accAsgTemplate('DETAILRATE3', accAsg.detailrate3);
		accAsgTemplate('DETAILRATE4', accAsg.detailrate4);
		accAsgTemplate('DETAILRATE5', accAsg.detailrate5);
		accAsgTemplate('DETAILRATE6', accAsg.detailrate6);
		accAsgTemplate('DETAILRATE7', accAsg.detailrate7);
		accAsgTemplate('DETAILRATE8', accAsg.detailrate8);
		accAsgTemplate('DETAILRATE9', accAsg.detailrate9);
		accAsgTemplate('DETAILRATE10', accAsg.detailrate10);
		accAsgTemplate('DETAILCURR1', accAsg.detailcurr1);
		accAsgTemplate('DETAILCURR2', accAsg.detailcurr2);
		accAsgTemplate('DETAILCURR3', accAsg.detailcurr3);
		accAsgTemplate('DETAILCURR4', accAsg.detailcurr4);
		accAsgTemplate('DETAILCURR5', accAsg.detailcurr5);
		accAsgTemplate('DETAILCURR6', accAsg.detailcurr6);
		accAsgTemplate('DETAILCURR7', accAsg.detailcurr7);
		accAsgTemplate('DETAILCURR8', accAsg.detailcurr8);
		accAsgTemplate('DETAILCURR9', accAsg.detailcurr9);
		accAsgTemplate('DETAILCURR10', accAsg.detailcurr10);
		accAsgTemplate('DETAILPER1', accAsg.detailper1);
		accAsgTemplate('DETAILPER2', accAsg.detailper2);
		accAsgTemplate('DETAILPER3', accAsg.detailper3);
		accAsgTemplate('DETAILPER4', accAsg.detailper4);
		accAsgTemplate('DETAILPER5', accAsg.detailper5);
		accAsgTemplate('DETAILPER6', accAsg.detailper6);
		accAsgTemplate('DETAILPER7', accAsg.detailper7);
		accAsgTemplate('DETAILPER8', accAsg.detailper8);
		accAsgTemplate('DETAILPER9', accAsg.detailper9);
		accAsgTemplate('DETAILPER10', accAsg.detailper10);
		accAsgTemplate('DETAILUOM1', accAsg.detailuom1);
		accAsgTemplate('DETAILUOM2', accAsg.detailuom2);
		accAsgTemplate('DETAILUOM3', accAsg.detailuom3);
		accAsgTemplate('DETAILUOM4', accAsg.detailuom4);
		accAsgTemplate('DETAILUOM5', accAsg.detailuom5);
		accAsgTemplate('DETAILUOM6', accAsg.detailuom6);
		accAsgTemplate('DETAILUOM7', accAsg.detailuom7);
		accAsgTemplate('DETAILUOM8', accAsg.detailuom8);
		accAsgTemplate('DETAILUOM9', accAsg.detailuom9);
		accAsgTemplate('DETAILUOM10', accAsg.detailuom10);
		accAsgTemplate('DETAILVENDOR1', accAsg.detailvendor1);
		accAsgTemplate('DETAILVENDOR2', accAsg.detailvendor2);
		accAsgTemplate('DETAILVENDOR3', accAsg.detailvendor3);
		accAsgTemplate('DETAILVENDOR4', accAsg.detailvendor4);
		accAsgTemplate('DETAILVENDOR5', accAsg.detailvendor5);
		accAsgTemplate('DETAILVENDOR6', accAsg.detailvendor6);
		accAsgTemplate('DETAILVENDOR7', accAsg.detailvendor7);
		accAsgTemplate('DETAILVENDOR8', accAsg.detailvendor8);
		accAsgTemplate('DETAILVENDOR9', accAsg.detailvendor9);
		accAsgTemplate('DETAILVENDOR10', accAsg.detailvendor10);
		accAsgTemplate('INCOTERMS1', accAsg.incoterms1);
		accAsgTemplate('INCOTERMS2', accAsg.incoterms2);
		accAsgTemplate('PRREFLINE', accAsg.prrefline);
		accAsgTemplate('CRITERIA1', accAsg.criteria1);
		accAsgTemplate('DEPARTMENT', accAsg.department);
		accAsgTemplate('REMARK', accAsg.headerremark);
		accAsgTemplate('REMARK1', accAsg.headerremark1);
		accAsgTemplate('REMARK2', accAsg.headerremark2);
		accAsgTemplate('REMARK3', accAsg.headerremark3);
		accAsgTemplate('NOTE', accAsg.headernote);
		accAsgTemplate('CONTACTNUM', accAsg.contactnumber);
		accAsgTemplate('REQUESTER', accAsg.requester);
		accAsgTemplate('SAVING_METHOD', accAsg.savingmethod);
		accAsgTemplate('YOUR_REF', accAsg.yourreference);
		accAsgTemplate('SHIPPING_INST', accAsg.shippinginstruction);
		accAsgTemplate('TERMS_OR_PAYMENT', accAsg.termofpayment);
		accAsgTemplate('OTHER_TERMS_COND', accAsg.othertermscondition);
		accAsgTemplate('OVER_DELIV', accAsg.overdelivtol);
		accAsgTemplate('UNDER_DELIV', accAsg.underdelivtol);
		accAsgTemplate('MAT_PO_TEXT', accAsg.matpotext);
		accAsgTemplate('SHIPPING_CHANNEL', accAsg.shippingchannel);
		accAsgTemplate('NET_WEIGHT', accAsg.netweight);
		accAsgTemplate('GROSS_WEIGHT', accAsg.grossweight);
		accAsgTemplate('UNIT_WEIGHT', accAsg.unitweight);
		accAsgTemplate('SUB_NUMBER', accAsg.subnumber);
		accAsgTemplate('CR', accAsg.cr);
		accAsgTemplate('REASON', accAsg.reason);
		accAsgTemplate('LEGAL_FLAG', accAsg.legalcontractflag);
		accAsgTemplate('LEGAL_CONTRACT_NO', accAsg.legalcontractno);
		accAsgTemplate('UNLOAD_PT', accAsg.unloadingpoint);
		accAsgTemplate('CAUNIT', 'ML');
		iDataTableItems.push(nextTmp);
	}
	requestBody = {
		I_DATA_TABLE: {
			item: iDataTableItems,
		},
		E_MESSAGES: eMessages,
		E_DATA_TABLE: eDataTable,
	};
	return requestBody;
};

module.exports = { createPoRequest };
