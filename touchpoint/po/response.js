function transform(proxyRes) {
	var responseBody = JSON.parse(proxyRes.getResultResponseBodyAsString());
	var handleSymbol = (message) => {
		return message.split('<').join('&lt;').split('>').join('&gt;');
	};
	var handleResponseType = (message) => {
		if (eMessageItem.search('Error') > -1) {
			return 'error';
		} else if (eMessageItem.search('Information') > -1) {
			return 'information';
		} else {
			return 'warning';
		}
	};
	var isSuccess = 'false';
	var responseType = 'error';
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			sendPoReply: '"sendporeply"',
			messages: '"messages"',
			metaData: '"metadata"',
		},
		record: {
			success: '"success"',
			poNum: '"ponum"',
			lineNum: '"linenum"',
			type: '"type"',
			code: '"code"',
			description: '"description"',
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.sendPoReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.poNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			let code = eMessageItem.match(/\(([^)]+)\)/) === null ? '' : eMessageItem.match(/\(([^)]+)\)/)[1].trim();
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
			messageBody = '';
		}
	} else {
		isSuccess = 'true';
		responseType = 'information';
		var poNum = responseBody.E_DATA_TABLE.item.find((item) => {
			return item.FIELD_NAME === 'PO_NUM';
		});
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.poNum + '><value>' + reqNum.FIELD_VALUE + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			let code = eMessageItem.match(/\(([^)]+)\)/) === null ? '' : eMessageItem.match(/\(([^)]+)\)/)[1].trim();
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
			messageBody = '';
		}
	}
	xmlBody += '</recordset></recordsetList>';
	var respsonse = { data: xmlBody };
	proxyRes.setResultResponseBody(JSON.stringify(respsonse));
}

const responsePo = (responseBody) => {
	var handleResponseType = (message) => {
		if (eMessageItem.search('Error') > -1) {
			return 'error';
		} else if (eMessageItem.search('Information') > -1) {
			return 'information';
		} else {
			return 'warning';
		}
	};
	var isSuccess = 'false';
	var responseType = 'error';
	var xmlConfig = { version: '"1.0"', enCoding: '"UTF-8"', standAlone: '"no"' };
	var recordsetListConfig = {
		recordset: {
			sendPoReply: '"sendporeply"',
			messages: '"messages"',
		},
		record: {
			success: '"success"',
			poNum: '"ponum"',
			lineNum: '"linenum"',
			type: '"type"',
			code: '"code"',
			description: '"description"',
		},
	};
	var xmlBody = '<?xml version=' + xmlConfig.version + ' encoding=' + xmlConfig.enCoding + ' standalone=' + xmlConfig.standAlone + '?><recordsetList>';
	xmlBody += '<recordset name=' + recordsetListConfig.recordset.sendPoReply + '><record>';
	if (responseBody.E_SUCCESS === '') {
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.poNum + '><value/></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			let code = eMessageItem.match(/\(([^)]+)\)/) === null ? '' : eMessageItem.match(/\(([^)]+)\)/)[1].trim();
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
			messageBody = '';
		}
	} else {
		isSuccess = 'true';
		responseType = 'information';
		var poNum = responseBody.E_DATA_TABLE.item.find((item) => {
			return item.FIELD_NAME === 'PO_NUM';
		});
		xmlBody += '<field name=' + recordsetListConfig.record.success + '><value>' + isSuccess + '</value></field>';
		xmlBody += '<field name=' + recordsetListConfig.record.poNum + '><value>' + reqNum.FIELD_VALUE + '</value></field>';
		xmlBody += '</record></recordset>';
		xmlBody += '<recordset name=' + recordsetListConfig.recordset.messages + '>';
		for (var z = 0; z < responseBody.E_MESSAGES.item.length; z++) {
			var messageBody = '';
			var eMessageItem = handleSymbol(responseBody.E_MESSAGES.item[z].ZMESSAGE);
			let code = eMessageItem.match(/\(([^)]+)\)/) === null ? '' : eMessageItem.match(/\(([^)]+)\)/)[1].trim();
			messageBody += '<record>';
			messageBody += '<field name=' + recordsetListConfig.record.lineNum + '><null/></field>';
			messageBody += '<field name=' + recordsetListConfig.record.type + '><value>' + responseType + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.code + '><value>' + code + '</value></field>';
			messageBody += '<field name=' + recordsetListConfig.record.description + '><value>' + eMessageItem + '</value></field>';
			messageBody += '</record>';
			xmlBody += messageBody;
			messageBody = '';
		}
	}
	xmlBody += '</recordset></recordsetList>';
	return xmlBody;
};
